def skipRemainingStages = false
def ACTION = ""
def comparecommit(){
	def GITACTION = ""
	GITACTION = sh(returnStdout: true, script:  
		 '''
				. helper/helper.yaml

				if [ $previouscommit == '$GIT_COMMIT' ]
				#if [ '$GIT_COMMIT' == '$GIT_COMMIT' ]
				then
					action="nako"
				else
					action="chalo"
				fi
				echo $action
		''')
	return(GITACTION)
	
}

def updateFixVersion(secrentName, jiraTickets, version){

stage('Update FIXVERSION'){
	
	withCredentials([usernamePassword(credentialsId: "${secrentName}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
		env.TICKETS="${jiraTickets}"
		env.FIXVERSION="${version}"

	sh '''
		#!/bin/bash
		jiraId+=( "$TICKETS" )
		for i in ${jiraId[@]}
		do
			echo "Updated Ticket $i"
			curl -D- -sk -u "admin":"admin" -X PUT --data '{"update":{"fixVersions":[{"set": [ {"name":"'$FIXVERSION'"} ] }]}}' -H "Content-Type: application/json" http://220.255.59.46:5050/rest/api/2/issue/$i
		done
		'''
	}

  }
}



pipeline{
	agent any

	stages{

		stage ('Call Jira'){
			steps{
				script{
					updateFixVersion('dipinthomas', 'REL-7 REL-8', '1.1.1')
				}
			}
		}
	    
/*	    stage ('Exit stage'){
			steps{
				script{
					
					if (GIT_PREVIOUS_SUCCESSFUL_COMMIT == GIT_COMMIT){
						skipRemainingStages = true
						println "Exit to aborted"
						currentBuild.result = 'ABORTED'
                    	return
					}

			}
		}
	}


		stage('Check Git Change'){
			when {
                expression {
                    !skipRemainingStages
                }
            }
			steps{
				script{
					ACTION = comparecommit()
					sh 'printenv'
					echo "Action from script block $ACTION"
				}
			}
		}

			stage ('True stage'){
			when {
                expression {
                    !skipRemainingStages
                }
            }

			steps{
				script{
					println "Continue Deployment"
					//currentBuild.result = 'ABORTED'
                    //return

			}
		}
	}

	stage ('Trigger UAT Deployment'){
			steps{
				script{
					if (ACTION == 'nako'){
					}
					sh '''
						  echo "CONTINUE to trigger the job"

						 '''

				}
			}
		}*/
}

post{

	aborted{
		echo "Hello from aborted"
		}
	}

}

