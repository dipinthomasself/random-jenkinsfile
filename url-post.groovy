/*

Post in groovy
--------------

To run this in jenkinsfile we will need lot of permission which can open up security risk.
Hence this is not being considered.

*/

def baseUrl = new URL('http://jenkins.clofest.com:5000')
def queryString = 'q=groovy&format=json&pretty=1'
def connection = baseUrl.openConnection()
connection.with {
  doOutput = true
  requestMethod = 'POST'
  outputStream.withWriter { writer ->
    writer << queryString
  }
  println content.text
}

