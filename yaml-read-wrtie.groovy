def skipRemainingStages = false

pipeline {
    agent any

    stages {
        stage("Stage 1") {
            when {
                expression {
                    !skipRemainingStages
                }
            }

            steps {
                script {
                    println "skipRemainingStages = ${skipRemainingStages}"
                }
            }
        }

        stage("Stage 2") {

            when {
                expression {
                    !skipRemainingStages
                }
            }

            steps {
                script {
                    println "This will show up...."

                }
            }
        }


        stage("Stage 3") {
            when {
                expression {
                    !skipRemainingStages
                }
            }

            steps {
                script {
                    println "This will wont show up...."
                    skipRemainingStages = true
                }
            }
        }


        stage("Stage 4") {
            when {
                expression {
                    !skipRemainingStages
                }
            }

            steps {
                script {
                    println "This text wont show up...."
                }
            }
        }



        stage("Stage 5") {
         when {
                expression {
                    !skipRemainingStages
                }
            }


            steps {
                script {
                    println "This text wont show up...."
                }
            }
        }
    }
}